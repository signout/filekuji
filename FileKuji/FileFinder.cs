﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Utility;

namespace FileKuji
{
    class FileFinder
    {
        List<string> Foundfiles = new List<string>();

        public FileFinder(IMainFormEventProvider p) {
            SetDefault();
            p.OnTargetFolderChanged += (s, e) => { TargetFolder = e.Text; ReFind(); };
            p.OnRecursiveChecked += (s, e) => { RecursiveChecked = e.Check; ReFind(); };
            p.OnWildcardSelectedChanged += (s, e) => ReFind();
            p.OnWildcardTextChanged += (s, e) => { WildcardPattern = e.Text; ReFind(); };
            p.OnWildcardComboBoxClosed += (s, e) => ReFind();
            p.OnFindBehaviorCheckChanged += OnFindBehaviorCheckChanged;
        }

        string TargetFolder;
        bool RecursiveChecked;
        string WildcardPattern;
        bool FindBehaviorChecked;

        void OnFindBehaviorCheckChanged(object sender, ButtonCheckedEventArgs e) {
            FindBehaviorChecked = e.Check;
            if (e.Check) {
                LastFound();
            }
            else {
                ReFind();
            }
            if (Foundfiles.Count == 0) {
                ReFind();
            }
        }

        #region FindActionManipulator
        Func<bool> Find_;
        public Func<bool> Find { get { return Find_; } internal set { Find_ = value; } }
        Func<bool> Nop() {
            return () => { return false; };
        }
        void SetDefault() {
            Find_ = Nop();
        }
        void LastFound() {
            Find_ = () => { return Foundfiles.Count != 0; };
        }
        void ReFind() {
            Find_ = () => FindFiles();
        }
        #endregion

        bool FindFiles() {
            if (string.IsNullOrWhiteSpace(TargetFolder))
                return false;

            try {
                Foundfiles.Clear();
                foreach (string pattern in Patterns()) {
                    string[] files = (Directory.GetFiles(TargetFolder, pattern.Trim(),
                        RecursiveChecked ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));
                    Foundfiles.InsertRange(Foundfiles.Count(), files);
                }
                if (FindBehaviorChecked)
                    LastFound();
                return Foundfiles.Count != 0;
            }
            catch (Exception e) {
                MessageBox.Show("エラーが発生しました。\n" + e.Message);
                return false;
            }
        }

        string[] Patterns() {
            return SearchPattern.Patterns(WildcardPattern);
        }

        public string Select() {
            return SearchPattern.Select<string>(Foundfiles, Foundfiles.Count());
        }
    }
}
