﻿namespace FileKuji
{
    public interface IMainFormEventProvider
    {
        event ControlEvent.TextChanged OnTargetFolderChanged;
        event ControlEvent.ButtonChecked OnRecursiveChecked;
        event ControlEvent.ItemChanged OnWildcardSelectedChanged;
        event ControlEvent.TextChanged OnWildcardTextChanged;
        event ControlEvent.OperationNotified OnWildcardComboBoxClosed;
        event ControlEvent.ButtonChecked OnFindBehaviorCheckChanged;
        event ControlEvent.ButtonChecked OnAutoExecuteCheckChanged;
    }
}
