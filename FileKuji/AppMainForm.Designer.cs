﻿namespace FileKuji
{
    partial class AppMainForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.TargetFolderGroupBox = new System.Windows.Forms.GroupBox();
            this.ReferredFolderButton = new System.Windows.Forms.Button();
            this.TargetFolderTextBox = new System.Windows.Forms.TextBox();
            this.OptionalGroupBox = new System.Windows.Forms.GroupBox();
            this.WildcardLabel = new System.Windows.Forms.Label();
            this.RecursiveCheckBox = new System.Windows.Forms.CheckBox();
            this.WildcardComboBox = new System.Windows.Forms.ComboBox();
            this.ResultGroupBox = new System.Windows.Forms.GroupBox();
            this.OpenFolderButton = new System.Windows.Forms.Button();
            this.AutoExecuteCheckBox = new System.Windows.Forms.CheckBox();
            this.FoundFileTextBox = new System.Windows.Forms.TextBox();
            this.StartButton = new System.Windows.Forms.Button();
            this.FindBehaviorCheckBox = new System.Windows.Forms.CheckBox();
            this.KujuMenuStrip = new System.Windows.Forms.MenuStrip();
            this.MenuItemViewRoot = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuItemViewStatistics = new System.Windows.Forms.ToolStripMenuItem();
            this.FavoriteMark = new System.Windows.Forms.PictureBox();
            this.RewindButton = new System.Windows.Forms.PictureBox();
            this.TargetFolderGroupBox.SuspendLayout();
            this.OptionalGroupBox.SuspendLayout();
            this.ResultGroupBox.SuspendLayout();
            this.KujuMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FavoriteMark)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RewindButton)).BeginInit();
            this.SuspendLayout();
            // 
            // TargetFolderGroupBox
            // 
            this.TargetFolderGroupBox.Controls.Add(this.ReferredFolderButton);
            this.TargetFolderGroupBox.Controls.Add(this.TargetFolderTextBox);
            this.TargetFolderGroupBox.Location = new System.Drawing.Point(13, 28);
            this.TargetFolderGroupBox.Name = "TargetFolderGroupBox";
            this.TargetFolderGroupBox.Size = new System.Drawing.Size(459, 50);
            this.TargetFolderGroupBox.TabIndex = 1;
            this.TargetFolderGroupBox.TabStop = false;
            this.TargetFolderGroupBox.Text = "対象フォルダ(&F)";
            // 
            // ReferredFolderButton
            // 
            this.ReferredFolderButton.Location = new System.Drawing.Point(382, 17);
            this.ReferredFolderButton.Name = "ReferredFolderButton";
            this.ReferredFolderButton.Size = new System.Drawing.Size(62, 23);
            this.ReferredFolderButton.TabIndex = 1;
            this.ReferredFolderButton.Text = "参照(&R)...";
            this.ReferredFolderButton.UseVisualStyleBackColor = true;
            this.ReferredFolderButton.Click += new System.EventHandler(this.ReferredFolderButton_Click);
            // 
            // TargetFolderTextBox
            // 
            this.TargetFolderTextBox.Location = new System.Drawing.Point(15, 19);
            this.TargetFolderTextBox.Name = "TargetFolderTextBox";
            this.TargetFolderTextBox.Size = new System.Drawing.Size(361, 19);
            this.TargetFolderTextBox.TabIndex = 0;
            this.TargetFolderTextBox.TextChanged += new System.EventHandler(this.TargetFolderTextBox_TextChanged);
            // 
            // OptionalGroupBox
            // 
            this.OptionalGroupBox.Controls.Add(this.WildcardLabel);
            this.OptionalGroupBox.Controls.Add(this.RecursiveCheckBox);
            this.OptionalGroupBox.Controls.Add(this.WildcardComboBox);
            this.OptionalGroupBox.Location = new System.Drawing.Point(13, 84);
            this.OptionalGroupBox.Name = "OptionalGroupBox";
            this.OptionalGroupBox.Size = new System.Drawing.Size(459, 50);
            this.OptionalGroupBox.TabIndex = 2;
            this.OptionalGroupBox.TabStop = false;
            this.OptionalGroupBox.Text = "オプション";
            // 
            // WildcardLabel
            // 
            this.WildcardLabel.AutoSize = true;
            this.WildcardLabel.Location = new System.Drawing.Point(183, 21);
            this.WildcardLabel.Name = "WildcardLabel";
            this.WildcardLabel.Size = new System.Drawing.Size(87, 12);
            this.WildcardLabel.TabIndex = 2;
            this.WildcardLabel.Text = "ワイルドカード(&W)";
            // 
            // RecursiveCheckBox
            // 
            this.RecursiveCheckBox.AutoSize = true;
            this.RecursiveCheckBox.Location = new System.Drawing.Point(15, 20);
            this.RecursiveCheckBox.Name = "RecursiveCheckBox";
            this.RecursiveCheckBox.Size = new System.Drawing.Size(155, 16);
            this.RecursiveCheckBox.TabIndex = 0;
            this.RecursiveCheckBox.Text = "サブフォルダも対象にする(&C)";
            this.RecursiveCheckBox.UseVisualStyleBackColor = true;
            this.RecursiveCheckBox.CheckedChanged += new System.EventHandler(this.RecursiveCheckBox_CheckedChanged);
            // 
            // WildcardComboBox
            // 
            this.WildcardComboBox.FormattingEnabled = true;
            this.WildcardComboBox.Items.AddRange(new object[] {
            "*",
            "*.gif; *.jpg; *.png",
            "*.mid; *.mp3; *.ogg",
            "*.avi; *.mov; *.mpg; *.mpeg; *.rm; *.wmv",
            "*.swf",
            "*.txt; *.htm; *.html",
            "*.doc; *.xls; *.pdf",
            "*.c; *.cpp; *.h",
            "*.lzh; *.zip",
            "*.exe"});
            this.WildcardComboBox.Location = new System.Drawing.Point(276, 18);
            this.WildcardComboBox.Name = "WildcardComboBox";
            this.WildcardComboBox.Size = new System.Drawing.Size(168, 20);
            this.WildcardComboBox.TabIndex = 2;
            this.WildcardComboBox.SelectedIndexChanged += new System.EventHandler(this.WildcardComboBox_SelectedIndexChanged);
            this.WildcardComboBox.DropDownClosed += new System.EventHandler(this.WildcardComboBox_DropDownClosed);
            this.WildcardComboBox.TextChanged += new System.EventHandler(this.WildcardComboBox_TextChanged);
            // 
            // ResultGroupBox
            // 
            this.ResultGroupBox.Controls.Add(this.OpenFolderButton);
            this.ResultGroupBox.Controls.Add(this.AutoExecuteCheckBox);
            this.ResultGroupBox.Controls.Add(this.FoundFileTextBox);
            this.ResultGroupBox.Location = new System.Drawing.Point(13, 182);
            this.ResultGroupBox.Name = "ResultGroupBox";
            this.ResultGroupBox.Size = new System.Drawing.Size(459, 70);
            this.ResultGroupBox.TabIndex = 3;
            this.ResultGroupBox.TabStop = false;
            this.ResultGroupBox.Text = "引いたファイル";
            // 
            // OpenFolderButton
            // 
            this.OpenFolderButton.Location = new System.Drawing.Point(382, 16);
            this.OpenFolderButton.Name = "OpenFolderButton";
            this.OpenFolderButton.Size = new System.Drawing.Size(62, 23);
            this.OpenFolderButton.TabIndex = 2;
            this.OpenFolderButton.Text = "開く(&O)";
            this.OpenFolderButton.UseVisualStyleBackColor = true;
            this.OpenFolderButton.Click += new System.EventHandler(this.OpenFolderButton_Click);
            // 
            // AutoExecuteCheckBox
            // 
            this.AutoExecuteCheckBox.AutoSize = true;
            this.AutoExecuteCheckBox.Location = new System.Drawing.Point(15, 48);
            this.AutoExecuteCheckBox.Name = "AutoExecuteCheckBox";
            this.AutoExecuteCheckBox.Size = new System.Drawing.Size(145, 16);
            this.AutoExecuteCheckBox.TabIndex = 3;
            this.AutoExecuteCheckBox.Text = "引いたときに自動実行(&A)";
            this.AutoExecuteCheckBox.UseVisualStyleBackColor = true;
            this.AutoExecuteCheckBox.CheckedChanged += new System.EventHandler(this.AutoExecuteCheckBox_CheckedChanged);
            // 
            // FoundFileTextBox
            // 
            this.FoundFileTextBox.Location = new System.Drawing.Point(15, 18);
            this.FoundFileTextBox.Name = "FoundFileTextBox";
            this.FoundFileTextBox.ReadOnly = true;
            this.FoundFileTextBox.Size = new System.Drawing.Size(361, 19);
            this.FoundFileTextBox.TabIndex = 1;
            this.FoundFileTextBox.TabStop = false;
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(177, 140);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(130, 36);
            this.StartButton.TabIndex = 0;
            this.StartButton.Text = "ファイルを引く";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // FindBehaviorCheckBox
            // 
            this.FindBehaviorCheckBox.AutoSize = true;
            this.FindBehaviorCheckBox.Enabled = false;
            this.FindBehaviorCheckBox.Location = new System.Drawing.Point(28, 151);
            this.FindBehaviorCheckBox.Name = "FindBehaviorCheckBox";
            this.FindBehaviorCheckBox.Size = new System.Drawing.Size(126, 16);
            this.FindBehaviorCheckBox.TabIndex = 3;
            this.FindBehaviorCheckBox.Text = "再検索を行わない(&S)";
            this.FindBehaviorCheckBox.UseVisualStyleBackColor = true;
            this.FindBehaviorCheckBox.CheckedChanged += new System.EventHandler(this.FindBehaviorCheckBox_CheckedChanged);
            this.FindBehaviorCheckBox.EnabledChanged += new System.EventHandler(this.FindBehaviorCheckBox_EnabledChanged);
            // 
            // KujuMenuStrip
            // 
            this.KujuMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemViewRoot});
            this.KujuMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.KujuMenuStrip.Name = "KujuMenuStrip";
            this.KujuMenuStrip.Size = new System.Drawing.Size(484, 24);
            this.KujuMenuStrip.TabIndex = 4;
            this.KujuMenuStrip.Text = "メニュー";
            // 
            // MenuItemViewRoot
            // 
            this.MenuItemViewRoot.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuItemViewStatistics});
            this.MenuItemViewRoot.Name = "MenuItemViewRoot";
            this.MenuItemViewRoot.Size = new System.Drawing.Size(61, 20);
            this.MenuItemViewRoot.Text = "表示(&V)";
            // 
            // MenuItemViewStatistics
            // 
            this.MenuItemViewStatistics.Name = "MenuItemViewStatistics";
            this.MenuItemViewStatistics.Size = new System.Drawing.Size(140, 22);
            this.MenuItemViewStatistics.Text = "統計情報(&S)";
            this.MenuItemViewStatistics.Click += new System.EventHandler(this.MenuItemViewStatistics_Click);
            // 
            // FavoriteMark
            // 
            this.FavoriteMark.Location = new System.Drawing.Point(457, 0);
            this.FavoriteMark.Name = "FavoriteMark";
            this.FavoriteMark.Size = new System.Drawing.Size(27, 27);
            this.FavoriteMark.TabIndex = 5;
            this.FavoriteMark.TabStop = false;
            this.FavoriteMark.Click += new System.EventHandler(this.FavoriteMark_Click);
            this.FavoriteMark.Paint += new System.Windows.Forms.PaintEventHandler(this.FavoriteMark_Paint);
            // 
            // RewindButton
            // 
            this.RewindButton.Location = new System.Drawing.Point(430, 0);
            this.RewindButton.Name = "RewindButton";
            this.RewindButton.Size = new System.Drawing.Size(27, 27);
            this.RewindButton.TabIndex = 6;
            this.RewindButton.TabStop = false;
            this.RewindButton.Click += new System.EventHandler(this.RewindButton_Click);
            this.RewindButton.Paint += new System.Windows.Forms.PaintEventHandler(this.RewindButton_Paint);
            // 
            // AppMainForm
            // 
            this.AcceptButton = this.StartButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 261);
            this.Controls.Add(this.RewindButton);
            this.Controls.Add(this.FavoriteMark);
            this.Controls.Add(this.FindBehaviorCheckBox);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.ResultGroupBox);
            this.Controls.Add(this.OptionalGroupBox);
            this.Controls.Add(this.TargetFolderGroupBox);
            this.Controls.Add(this.KujuMenuStrip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.KujuMenuStrip;
            this.Name = "AppMainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "ファイルくじ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.AppMainForm_FormClosing);
            this.TargetFolderGroupBox.ResumeLayout(false);
            this.TargetFolderGroupBox.PerformLayout();
            this.OptionalGroupBox.ResumeLayout(false);
            this.OptionalGroupBox.PerformLayout();
            this.ResultGroupBox.ResumeLayout(false);
            this.ResultGroupBox.PerformLayout();
            this.KujuMenuStrip.ResumeLayout(false);
            this.KujuMenuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FavoriteMark)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RewindButton)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox TargetFolderGroupBox;
        private System.Windows.Forms.Button ReferredFolderButton;
        private System.Windows.Forms.TextBox TargetFolderTextBox;
        private System.Windows.Forms.GroupBox OptionalGroupBox;
        private System.Windows.Forms.ComboBox WildcardComboBox;
        private System.Windows.Forms.Label WildcardLabel;
        private System.Windows.Forms.CheckBox RecursiveCheckBox;
        private System.Windows.Forms.GroupBox ResultGroupBox;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.CheckBox FindBehaviorCheckBox;
        private System.Windows.Forms.CheckBox AutoExecuteCheckBox;
        private System.Windows.Forms.TextBox FoundFileTextBox;
        private System.Windows.Forms.MenuStrip KujuMenuStrip;
        private StatisticsForm StatisticsInfoForm;
        private System.Windows.Forms.ToolStripMenuItem MenuItemViewRoot;
        private System.Windows.Forms.ToolStripMenuItem MenuItemViewStatistics;
        private System.Windows.Forms.PictureBox FavoriteMark;
        private System.Windows.Forms.PictureBox RewindButton;
        private System.Windows.Forms.Button OpenFolderButton;
    }
}

