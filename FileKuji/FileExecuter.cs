﻿using System;
using System.IO;
using System.Windows.Forms;

namespace FileKuji
{
    class FileExecuter
    {
        bool AutoChecked;

        public FileExecuter(IMainFormEventProvider p) {
            p.OnAutoExecuteCheckChanged += OnStatusChanged;
        }

        void OnStatusChanged(object sender, ButtonCheckedEventArgs e) {
            AutoChecked = e.Check;
        }

        public void Execute(string f) {
            if (!AutoChecked)
                return;
            if (!File.Exists(f))
                return;
            Start(f);
        }

        void Start(string f) {
            try {
                System.Diagnostics.Process.Start(f);
            }
            catch (Exception ex) {
                MessageBox.Show("エラーが発生しました。\n" + ex.Message);
            }
        }
    }
}
