﻿using System;
using System.Collections.Generic;
using System.Data.SqlServerCe;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileKuji
{
    public class FreqItemSqlCeDB : FreqItemDB
    {
        FreqItems Statistics = new FreqItems();

        #region FreqItemDB Members
        public void Update(string itemName) {
            var conn = Connect();
            using (var adapter = new SqlCeDataAdapter()) {
                SetupSelectCommand(conn, adapter);
                adapter.Fill(Statistics.StatItem);
                RegisterOrIncrementKujiCount(adapter, itemName);
            }
        }

        public void Like(string itemName) {
            var conn = Connect();
            using (var adapter = new SqlCeDataAdapter()) {
                SetupSelectCommand(conn, adapter);
                adapter.Fill(Statistics.StatItem);
                var x = Utility.StringDigest.PathAndUniqId(itemName);
                var favRow = Statistics.StatItem.FindByUniqId(x.Item2);
                if (favRow != null) {
                    if (favRow.Favorite < short.MaxValue) {             // XXX
                        favRow.Favorite++;
                        adapter.Update(Statistics.StatItem);
                    }
                }
            }
        }

        public FreqItems Query() {
            if (Statistics.StatItem.Count <= 0) {
                var conn = Connect();
                using (var adapter = new SqlCeDataAdapter()) {
                    SetupSelectCommand(conn, adapter);
                    adapter.Fill(Statistics.StatItem);
                }
            }
            //return (FreqItems)Statistics.Clone();                         // XXX
            return Statistics;
        }
        #endregion

        SqlCeConnection Connect() {
            var conn = new SqlCeConnection();
            var builder = new SqlCeConnectionStringBuilder();
            builder["Data Source"] = DBName;
            conn.ConnectionString = builder.ConnectionString;
            return conn;
        }

        void SetupSelectCommand(SqlCeConnection connection, SqlCeDataAdapter adapter) {
            var cmdBuilder = new SqlCeCommandBuilder(adapter);
            var cmd_select = new SqlCeCommand();
            cmd_select.Connection = connection;
            cmd_select.CommandText = "SELECT * FROM Freq";
            adapter.SelectCommand = cmd_select;
        }

        void RegisterOrIncrementKujiCount(SqlCeDataAdapter adapter, string itemName) {
            var x = Utility.StringDigest.PathAndUniqId(itemName);
            var itemRow = Statistics.StatItem.FindByUniqId(x.Item2);
            if (itemRow != null) {
                /* Increment */
                itemRow.KujiCount++;
                itemRow.LastTime = DateTime.Now;
                adapter.Update(Statistics.StatItem);
            }
            else {
                /* Register */
                var row = Statistics.StatItem.NewStatItemRow();
                row.UniqId = x.Item2;
                row.Name = x.Item1;
                var now = DateTime.Now;
                row.RegisterTime = now;
                row.LastTime = now;
                row.Favorite = 0;
                row.KujiCount = 1;
                Statistics.StatItem.Rows.Add(row);
                adapter.Update(Statistics.StatItem);
            }
        }

        static readonly string defaultDBName = @".\DB\statistics.sdf";
        string dbname = defaultDBName;
        public string DBName {
            get {
                return dbname;
            }
            set {
                if (!File.Exists(value)) {
                    throw new ArgumentException("File Not Found", value.ToString());
                }
                dbname = value;
            }
        }
    }
}
