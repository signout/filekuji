﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileKuji
{
    public interface FreqItemDB
    {
        void Update(string itemName);
        void Like(string itemName);
        FreqItems Query();
    }
}
