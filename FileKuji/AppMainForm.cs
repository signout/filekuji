﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using IniFile;

namespace FileKuji
{
    public partial class AppMainForm : Form, IMainFormEventProvider
    {
        public AppMainForm() {
            InitializeFinder();
            InitializeComponent();
            InitializeComponentFromIniFile();
            InitializeIcons();
        }

        readonly string IniFilename = "filekuji.ini";
        FileFinder Finder;
        FileExecuter Executer;
        FreqItemDB FreqDB;
        Stack<string> RecentFileList = new Stack<string>();

        Image FavoriteImage;
        Image InitialImage;
        Image PressedImage;

        Image RewindImage;

        void AppMainForm_FormClosing(object sender, FormClosingEventArgs e) {
            WriteToIni();
        }

        #region IMainFormEventProvider メンバー
        public event ControlEvent.TextChanged OnTargetFolderChanged;
        public event ControlEvent.ButtonChecked OnRecursiveChecked;
        public event ControlEvent.ItemChanged OnWildcardSelectedChanged;
        public event ControlEvent.TextChanged OnWildcardTextChanged;
        public event ControlEvent.OperationNotified OnWildcardComboBoxClosed;
        public event ControlEvent.ButtonChecked OnFindBehaviorCheckChanged;
        public event ControlEvent.ButtonChecked OnAutoExecuteCheckChanged;
        #endregion

        void InitializeFinder() {
            Finder = new FileFinder(this);
            Executer = new FileExecuter(this);
            FreqDB = new FreqItemSqlCeDB();
        }

        void InitializeStatisticsForm() {
            if (StatisticsInfoForm == null) {
                StatisticsInfoForm = new StatisticsForm();
                StatisticsInfoForm.Load += StatisticsInfoForm_Load;
            }
        }

        void InitializeComponentFromIniFile() {
            TargetFolderTextBox.Text = IniFileHandler.ReadIniFile(
                IniFilename, "WindowData", "ObjectPath", Environment.GetEnvironmentVariable("userprofile"));
            Func<string, bool> toBool = x => x == "0" ? false : true;
            RecursiveCheckBox.Checked = toBool(
                IniFileHandler.ReadIniFile(IniFilename, "WindowData", "SearchSubFolder", "0"));
            WildcardComboBox.Text = IniFileHandler.ReadIniFile(IniFilename, "WindowData", "WildCard", "");
            FindBehaviorCheckBox.Checked = toBool(
                IniFileHandler.ReadIniFile(IniFilename, "WindowData", "FileAction", "0"));
            AutoExecuteCheckBox.Checked = toBool(
                IniFileHandler.ReadIniFile(IniFilename, "WindowData", "AutoAction", "1"));
        }

        void WriteToIni() {
            IniFileHandler.WriteIniFile(IniFilename, "WindowData", "ObjectPath", TargetFolderTextBox.Text);
            IniFileHandler.WriteIniFile(IniFilename, "WindowData", "SearchSubFolder", RecursiveCheckBox.Checked == false ? "0" : "1");
            IniFileHandler.WriteIniFile(IniFilename, "WindowData", "WildCard", WildcardComboBox.Text);
            IniFileHandler.WriteIniFile(IniFilename, "WindowData", "FileAction", FindBehaviorCheckBox.Checked == false ? "0" : "1");
            IniFileHandler.WriteIniFile(IniFilename, "WindowData", "AutoAction", AutoExecuteCheckBox.Checked == false ? "0" : "1");
        }

        void InitializeIcons() {
            InitializeFavoriteMark();
            InitializeRewindIcon();
        }

        void InitializeRewindIcon() {
            Assembly assembly = Assembly.GetExecutingAssembly();
            var x = assembly.GetManifestResourceStream("FileKuji.image.rewind.png");
            RewindImage = new Bitmap(x);
        }

        void InitializeFavoriteMark() {
            LoadFavoriteImages();
            FavoriteImage = InitialImage;
        }

        void LoadFavoriteImages() {
            Assembly assembly = Assembly.GetExecutingAssembly();
            var t = assembly.GetManifestResourceStream("FileKuji.image.unhappy.png");
            InitialImage = new Bitmap(t);
            var s = assembly.GetManifestResourceStream("FileKuji.image.happy.png");
            PressedImage = new Bitmap(s);
        }

        void TargetFolderTextBox_TextChanged(object sender, EventArgs e) {
            var x = sender as System.Windows.Forms.TextBox;
            if (OnTargetFolderChanged != null)
                OnTargetFolderChanged(this, new TextChangedEventArgs(x.Text));
        }

        void RecursiveCheckBox_CheckedChanged(object sender, EventArgs e) {
            var x = sender as System.Windows.Forms.CheckBox;
            if (OnRecursiveChecked != null)
                OnRecursiveChecked(this, new ButtonCheckedEventArgs(x.Checked));
        }

        /// <summary>
        /// リストアイテム文字列を編集したときの対策
        /// </summary>
        void WildcardComboBox_TextChanged(object sender, EventArgs e) {
            var x = sender as System.Windows.Forms.ComboBox;
            if (OnWildcardTextChanged != null)
                OnWildcardTextChanged(this, new TextChangedEventArgs(x.Text));
        }

        void WildcardComboBox_SelectedIndexChanged(object sender, EventArgs e) {
            var x = sender as System.Windows.Forms.ComboBox;
            if (OnWildcardSelectedChanged != null)
                OnWildcardSelectedChanged(this, new ItemChangedEventArgs(x.SelectedIndex));
        }

        /// <summary>
        /// リストアイテムの文字列を編集後にドロップダウンしてEscapeしたときの対策
        /// </summary>
        void WildcardComboBox_DropDownClosed(object sender, EventArgs e) {
            var x = sender as System.Windows.Forms.ComboBox;
            if (OnWildcardComboBoxClosed != null)
                OnWildcardComboBoxClosed(this, EventArgs.Empty);
        }

        void FindBehaviorCheckBox_EnabledChanged(object sender, EventArgs e) {
            var x = sender as System.Windows.Forms.CheckBox;
            if (OnFindBehaviorCheckChanged != null)
                OnFindBehaviorCheckChanged(this, new ButtonCheckedEventArgs(x.Checked));
        }

        void FindBehaviorCheckBox_CheckedChanged(object sender, EventArgs e) {
            var x = sender as System.Windows.Forms.CheckBox;
            if (OnFindBehaviorCheckChanged != null)
                OnFindBehaviorCheckChanged(this, new ButtonCheckedEventArgs(x.Checked));
        }

        void AutoExecuteCheckBox_CheckedChanged(object sender, EventArgs e) {
            var x = sender as System.Windows.Forms.CheckBox;
            if (OnAutoExecuteCheckChanged != null)
                OnAutoExecuteCheckChanged(this, new ButtonCheckedEventArgs(x.Checked));
        }

        void ReferredFolderButton_Click(object sender, EventArgs e) {
            var fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog(this) == DialogResult.OK) {
                TargetFolderTextBox.Text = fbd.SelectedPath;
            }
        }

        void MenuItemViewStatistics_Click(object sender, EventArgs e) {
            InitializeStatisticsForm();
            StatisticsInfoForm.Show();
        }

        void StatisticsInfoForm_Load(object sender, EventArgs e)
        {
            StatisticsInfoForm.BindDataSource(FreqDB.Query());
        }

        private void FavoriteMark_Paint(object sender, PaintEventArgs e) {
            e.Graphics.DrawImage(
                FavoriteImage,
                0, 0, FavoriteMark.Width, FavoriteMark.Height);
        }

        private void FavoriteMark_Click(object sender, EventArgs e) {
            FavoriteImage = PressedImage;
            FavoriteMark.Invalidate();
            FreqDB.Like(FoundFileTextBox.Text);
        }

        private void OpenFolderButton_Click(object sender, EventArgs e) {
            if (File.Exists(FoundFileTextBox.Text)) {
                Process.Start("explorer.exe", @"/n,/select," + FoundFileTextBox.Text);
            }
        }

        void RewindButton_Paint(object sender, PaintEventArgs e) {
            e.Graphics.DrawImage(
                RewindImage,
                0, 0, RewindButton.Width, RewindButton.Height);
        }

        void RewindButton_Click(object sender, EventArgs e) {
            if (RecentFileList.Count >= 2) {
                var _ = RecentFileList.Pop();
                FoundFileTextBox.Text = RecentFileList.Pop();
                FileInvoke(true);
            }
        }

        void StartButton_Click(object sender, EventArgs e) {
            bool f = Finder.Find();
            EnableFindBehavior(f);
            UpdateFoundFileTextBox(f);
            FileInvoke(f);
        }

        void EnableFindBehavior(bool b) {
            if (!b || FindBehaviorCheckBox.Enabled)
                return;
            FindBehaviorCheckBox.Enabled = true;
        }

        void UpdateFoundFileTextBox(bool b) {
            if (!b) {
                FoundFileTextBox.Text = "ファイルが見つかりません。";
                return;
            }
            FoundFileTextBox.Text = Finder.Select();
        }

        void FileInvoke(bool b) {
            if (b) {
                Executer.Execute(FoundFileTextBox.Text);
                RecentFileList.Push(FoundFileTextBox.Text);
                FreqDB.Update(FoundFileTextBox.Text);
                if (StatisticsInfoForm != null) {
                    StatisticsInfoForm.UpdateStatistics();
                }
                FavoriteImage = InitialImage;
                FavoriteMark.Invalidate();
            }
        }
    }
}