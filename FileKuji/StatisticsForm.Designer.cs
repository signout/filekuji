﻿namespace FileKuji
{
    partial class StatisticsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            this.StatItemDataGridView = new System.Windows.Forms.DataGridView();
            this.StatItemBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.StatItemDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatItemBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // StatItemDataGridView
            // 
            this.StatItemDataGridView.AllowUserToAddRows = false;
            this.StatItemDataGridView.AllowUserToDeleteRows = false;
            this.StatItemDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StatItemDataGridView.Location = new System.Drawing.Point(0, 0);
            this.StatItemDataGridView.Name = "StatItemDataGridView";
            this.StatItemDataGridView.ReadOnly = true;
            this.StatItemDataGridView.RowTemplate.Height = 21;
            this.StatItemDataGridView.Size = new System.Drawing.Size(745, 173);
            this.StatItemDataGridView.TabIndex = 0;
            // 
            // StatisticsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(745, 173);
            this.Controls.Add(this.StatItemDataGridView);
            this.Name = "StatisticsForm";
            this.Text = "統計情報";
            ((System.ComponentModel.ISupportInitialize)(this.StatItemDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.StatItemBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView StatItemDataGridView;
        private System.Windows.Forms.BindingSource StatItemBindingSource;
    }
}