﻿namespace IniFile
open System
open System.Runtime.InteropServices
open System.Text
module IniFileHandler =
    (*
      Top-level external function in module doesn't have private accessibilities?
    *)
    module private ExternalFunction =
        [<DllImport("kernel32.dll", CharSet = CharSet.Unicode)>]
        extern uint32
            GetPrivateProfileString(
                string lpAppName,
                string lpKeyName,
                string lpDefault,
                StringBuilder lpReturnedString,
                uint32 nSize,
                string lpFileName)
        [<DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)>]
        extern [<return: MarshalAs(UnmanagedType.Bool)>] bool
            WritePrivateProfileString(
                string lpAppName,
                string lpKeyName,
                string lpString,
                string lpFileName)
    let internal DefaultStringLength = 500
    /// <param name='ini'>inifilename</param>
    /// <param name='s'>section</param>
    /// <param name='k'>key</param>
    /// <param name='d'>default</param>
    let ReadIniFile ini s k d =
        let f = AppDomain.CurrentDomain.BaseDirectory + ini
        let mutable sb : StringBuilder = new StringBuilder(DefaultStringLength)
        let c = Convert.ToUInt32(sb.Capacity)
        let _ = ExternalFunction.GetPrivateProfileString(s, k, d, sb, c, f) // cannot remove parentheses, commas
        sb.ToString()
    /// <param name='ini'>inifilename</param>
    /// <param name='s'>section</param>
    /// <param name='k'>key</param>
    /// <param name='v'>value</param>
    let WriteIniFile ini s k v =
        let f = AppDomain.CurrentDomain.BaseDirectory + ini
        ExternalFunction.WritePrivateProfileString(s, k, v, f)