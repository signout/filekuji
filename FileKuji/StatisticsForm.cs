﻿using System;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace FileKuji
{
    public partial class StatisticsForm : Form
    {
        bool StatItemDataGridViewIsInitialized = false;
        public StatisticsForm() {
            InitializeComponent();
            StatItemDataGridView.DataError += StatItemDataGridView_DataError;
        }

        #region Laziness Setup GridView
        void SetupGridView() {
            StatItemDataGridView.AutoGenerateColumns = true;
            StatItemDataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            StatItemDataGridView.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            StatItemDataGridView.DataSource = StatItemBindingSource;
            StatItemDataGridView.DataMember = "StatItem";
            StatItemDataGridView.Columns.Remove("UniqId");
            StatItemDataGridViewIsInitialized = true;
        }

        void StatItemDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e) {
            if (StatItemDataGridViewIsInitialized)
                throw new NotImplementedException();
        }
        #endregion

        protected override void OnClosing(CancelEventArgs e) {
            this.Hide();
            e.Cancel = true;
            base.OnClosing(e);
        }

        FreqItems DataSource;
        public void BindDataSource(FreqItems source) {
            DataSource = source;
            StatItemBindingSource.DataSource = DataSource;
            SetupGridView();
            ReloadStatisticsItems();
        }

        public void UpdateStatistics() {
            if (DataSource != null) {
                ReloadStatisticsItems();
            }
        }

        void ReloadStatisticsItems() {
            //StatItemBindingSource.ResetBindings(false);
        }
    }
}
