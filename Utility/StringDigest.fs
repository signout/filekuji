﻿namespace Utility
module StringDigest =
  open System.Security.Cryptography
  open System.Text
  open System.IO

  let UniqId (x : string) =
    let b = Encoding.GetEncoding(936).GetBytes(x)
    let m = new SHA1Managed()
    m.ComputeHash(b)

  let PathAndUniqId (x : string) =
    let p = Path.GetFileName x
    let id = UniqId x
    (p, id)