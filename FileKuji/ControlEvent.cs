﻿using System;
namespace FileKuji
{
    public class ControlEvent
    {
        public delegate void TextChanged(object sender, TextChangedEventArgs e);
        public delegate void ButtonChecked(object sender, ButtonCheckedEventArgs e);
        public delegate void ItemChanged(object sender, ItemChangedEventArgs e);
        public delegate void OperationNotified(object sender, EventArgs e);
    }

    public class TextChangedEventArgs : EventArgs
    {
        public TextChangedEventArgs(string text) {
            this.text = text;
        }
        private string text;
        public string Text { get { return text; } }
    }

    public class ButtonCheckedEventArgs : EventArgs
    {
        public ButtonCheckedEventArgs(bool check) {
            this.check = check;
        }
        private bool check;
        public bool Check { get { return this.check; } }
    }

    public class ItemChangedEventArgs : EventArgs
    {
        public ItemChangedEventArgs(int index) {
            this.index = index;
        }
        private int index;
        public int Index { get { return this.index; } }
    }
}