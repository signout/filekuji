﻿namespace Utility
module SearchPattern  =
    let Patterns x =
        match System.String.IsNullOrEmpty(x) with
        | false -> x.Split(';')
        | true -> [|"*"|]
    let Select (xs : 'a seq) (n : int) =
        let rand = System.Random(int(System.DateTime.Now.Ticks) &&& 0x0000FFFF)
        let flip f x y = f y x
        rand.Next n |> flip Seq.nth xs